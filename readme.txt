- Requirements
Before you run the script you should check if you have all following installed:
* Python 3.7 (https://www.python.org/downloads/)
* pip 3 (https://pip.pypa.io/en/stable/installing/)
* Requests (http://docs.python-requests.org/en/master/user/install/)
* Inquirer (https://pypi.org/project/inquirer/)

- How to use
* Configuration
    Open the file 'config.py' and follow the instructions written in there.
* Execution
    Run 'main.py' file on Terminal (MacOS) or Command Prompt (Windows) with the following command:
    $ python3 main.py

- Description of the script
Once the script executes, it will search for all the projects in the instance. 
It will prompt the project list on the screen asking for the user to choose a project.
Then, it will search for all the Epics in that project. It will take each Epic and search for an Epick Link
in every project. All the issues linked to the Epic will have the time tracking information gathered, including its sub-tasks.
On the console, the script will always display a message of what it's doing. 
If an error occurs, it will display that message and continue executing, or it will end the execution.
When all the data is collected, and everything went fine, it will display this message: 'DONE'.

- Output
The CSV file generated will have the following structure:
Epic             
 - Sub-tasks (directly linked to the Epic)
 - Task/Story/Bug/Etc
  -Sub-tasks (Linked to the Task/Story/Bug/Etc)
All the time information is in seconds.

The file 'example.png' is an example of how the report displays the data.