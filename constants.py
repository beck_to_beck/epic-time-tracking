# main.py
# Epic Time Tracking
# constants.py
# Create by Fábio Beck Wanderer

# These values should remain as is.
FIELDS = ['summary', 'key',  'id', 'issuetype', 'subtasks', 'aggregateprogress',
          'aggregatetimeoriginalestimate', 'aggregatetimespent', 'progress', 'timeestimate', 'timespent', 'customfield_10011', 'customfield_10014', 'timeoriginalestimate', 'customfield_10002' ]
RESERVED_WORDS = ["abort", "access", "add", "after", "alias", "all", "alter", "and", "any", "as", "asc", "audit", "avg", "before", "begin", "between", "boolean", "break", "by", "byte", "catch", "cf", "char", "character", "check", "checkpoint", "collate", "collation", "column", "commit", "connect", "continue", "count", "create", "current", "date", "decimal", "declare", "decrement", "default", "defaults", "define", "delete", "delimiter", "desc", "difference", "distinct", "divide", "do", "double", "drop", "else", "empty", "encoding", "end", "equals", "escape", "exclusive", "exec", "execute", "exists", "explain", "false", "fetch", "file", "field", "first", "float", "for", "from", "function", "go", "goto", "grant", "greater", "group", "having", "identified", "if", "immediate", "in", "increment", "index", "initial", "inner",
                                   "inout", "input", "insert", "int", "integer", "intersect", "intersection", "into", "is", "isempty", "isnull", "join", "last", "left", "less", "like", "limit", "lock", "long", "max", "min", "minus", "mode", "modify", "modulo", "more", "multiply", "next", "noaudit", "not", "notin", "nowait", "null", "number", "object", "of", "on", "option", "or", "order", "outer", "output", "power", "previous", "prior", "privileges", "public", "raise", "raw", "remainder", "rename", "resource", "return", "returns", "revoke", "right", "row", "rowid", "rownum", "rows", "select", "session", "set", "share", "size", "sqrt", "start", "strict", "string", "subtract", "sum", "synonym", "table", "then", "to", "trans", "transaction", "trigger", "true", "uid", "union", "unique", "update", "user", "validate", "values", "view", "when", "whenever", "where", "while", "with"]

CSV_EXTENSION = ".csv"
WRITE_MODE = 'w'
MAX_RESULT = 100
HEADER = ['ID', 'Key', 'Issue Type', 'Summary',
          'Estimated', 'Logged', 'Total Estimated', 'Total Logged']
ENDPOINT_SEARCH = '/rest/api/3/search'
ENDPOINT_ISSUE = '/rest/api/3/issue/'
ENDPOINT_EPIC_LINK = '/rest/api/3/issueLink/'
ENDPOINT_PROJECT_SEARCH = '/rest/api/3/project/search'
ENDPOINT_PROJECT = '/rest/api/3/project/'
METHOD_POST =  'POST'
METHOD_GET = 'GET'
STATUS_CODE_OK = 200
FIELD_FIELDS = 'fields'
FIELD_ID = 'id'
FIELD_KEY = 'key'
FIELD_ISSUETYPE = 'issuetype'
FIELD_NAME = 'name'
FIELD_TIMEESTIMATE = 'timeestimate'
FIELD_TIMESPENT = 'timespent'
FIELD_AGTIMEOST = 'aggregatetimeoriginalestimate'
FIELD_AGTS = 'aggregatetimespent'
FIELD_TIMEOS = 'timeoriginalestimate'
FIELD_SUBTASK = 'subtask'
FIELD_SUBTASKS = 'subtasks'
FIELD_ISSUES = 'issues'
FIELD_TOTAL = 'total'
# This may very for every instance
FIELD_EPICLINK = 'customfield_10002'
FIELD_VALUES = 'values'
FIELD_SIMPLIFIED = 'simplified'
FIELD_ERROR_MESSAGES = 'errorMessages'
FIELD_SUMMARY = 'summary'
INSTANCE = 'instance'