# main.py
# Epic Time Tracking
# main.py
# Create by Fábio Beck Wanderer

from library.api_calls import ApiCalls
from library.helper import Helper
import config

# Main function
def main():
    helper = Helper()
    helper.createEpicReport()
    
if __name__ == '__main__':
    main()