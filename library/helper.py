# main.py
# Epic Time Tracking
# helper.py
# Create by Fábio Beck Wanderer

from library.api_calls import ApiCalls
import constants
import config
from library.ask_question import AskUser
import csv
import sys
import time

# Provide all the methods to search for issues in Epics
class Helper():
    api_call = ApiCalls()
    api_call.setCredentials(config.credential)
    api_call.setAuth('basic')

    # Main function of the Helper Class
    def createEpicReport(self):
        # Get the selected project key
        project = self.getProject()
        # The a list containing all the project keys
        projects = self.getProjects()

        # Check for error
        if type(project) == int:
            sys.stdout.write(
                '\x1b[2K\r' + "Something went wrong, error code: " + str(project) + "\n")
            sys.stdout.flush()
        else:
            # JQL query to search for Epics in the project
            jql = "project = " + project + " and issuetype = Epic"

            # Control variables for pagination
            start_at = 0
            diff = 100
            counter = 0

            # Create the csv report file
            with open(config.filename + ".csv", mode='w') as story_points_report:
                story_points_reporte_writer = csv.writer(
                    story_points_report, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
                story_points_reporte_writer.writerow(constants.HEADER)

                # Check for all the until there is no more pages
                while diff > 0:
                    json_query = {
                        'jql': jql,
                        'startAt': start_at,
                        'maxResults': constants.MAX_RESULT,
                        'fields': constants.FIELDS
                    }

                    sys.stdout.write('\x1b[2K\r' + "Searching for issues...")
                    sys.stdout.flush()

                    response = self.api_call.request(
                        constants.METHOD_POST, config.credential[constants.INSTANCE] + constants.ENDPOINT_SEARCH, json_data=json_query)

                    response_json = response.json()

                    status_code = response.status_code
                    # Check for error
                    if response.status_code != constants.STATUS_CODE_OK:
                        error = response.json()
                        sys.stdout.write(
                            '\x1b[2K\r' + 'Something went wrong: ' + str(status_code) + ": " + error[constants.FIELD_ERROR_MESSAGES][0] + "\n")
                        sys.stdout.flush()
                        diff = -1
                    else:
                        # All the issues that have an Epic Link
                        for issue in response_json[constants.FIELD_ISSUES]:
                            # Get all the issues inside the Epic
                            rows = self.getEpicLink(projects, issue)
                            # Write all the collected rows
                            story_points_reporte_writer.writerows(rows)
                        counter += 1

                    start_at = counter * constants.MAX_RESULT
                    diff = response_json[constants.FIELD_TOTAL] - start_at

            sys.stdout.write('\x1b[2K\r' + "DONE" + "\n")
            sys.stdout.flush()

    # Not used in this case, debugging only
    def createReport(self):
        project = self.getProject()

        if type(project) == int:
            sys.stdout.write(
                '\x1b[2K\r' + "Something went wrong, error code: " + str(project) + "\n")
            sys.stdout.flush()
        else:
            jql = "project = " + project

            start_at = 0
            diff = 100
            counter = 0

            with open(config.filename + constants.CSV_EXTENSION, mode=constants.WRITE_MODE) as story_points_report:
                story_points_reporte_writer = csv.writer(
                    story_points_report, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
                story_points_reporte_writer.writerow(constants.HEADER)

                while diff > 0:
                    json_query = {
                        'jql': jql,
                        'startAt': start_at,
                        'maxResults': constants.MAX_RESULT,
                        'fields': constants.FIELDS
                    }

                    sys.stdout.write('\x1b[2K\r' + "Searching for issues...")
                    sys.stdout.flush()

                    response = self.api_call.request(
                        constants.METHOD_POST, config.credential[constants.INSTANCE] + constants.ENDPOINT_SEARCH, json_data=json_query)

                    response_json = response.json()

                    status_code = response.status_code
                    if response.status_code != constants.STATUS_CODE_OK:
                        sys.stdout.write(
                            '\x1b[2K\r' + 'Something went wrong: ' + str(status_code) + "\n")
                        sys.stdout.flush()
                        diff = -1
                    else:
                        rows = self.getIssues(
                            config.credential[constants.INSTANCE], response_json[constants.FIELD_ISSUES], project)
                        story_points_reporte_writer.writerows(rows)
                        counter += 1

                    start_at = counter * constants.MAX_RESULT
                    diff = response_json[constants.FIELD_TOTAL] - start_at

            sys.stdout.write('\x1b[2K\r' + "DONE" + "\n")
            sys.stdout.flush()

    # Function to get all the project keys
    def getProjects(self):
        diff = 50
        start_at = 0

        project_keys = []

        counter = 0

        while diff > 0:
            response = self.api_call.request(
                constants.METHOD_GET, config.credential[constants.INSTANCE] + constants.ENDPOINT_PROJECT_SEARCH + "?startAt=" + str(start_at))
            response_json = response.json()

            status_code = response.status_code
            if response.status_code != constants.STATUS_CODE_OK:
                diff = -1
            else:
                values = response_json[constants.FIELD_VALUES]

                for info in values:
                    project_keys.append(info[constants.FIELD_KEY])

            counter += 1
            
            start_at = counter * 50
            diff = response_json[constants.FIELD_TOTAL] - start_at

        return project_keys

    # Function to list all project keys and its names, return the selected one
    def getProject(self):
        diff = 50
        start_at = 0

        previous_start = 0
        previous_diff = 50

        project_dict = {}
        project_names = []

        output = None
        counter = 0
        sys.stdout.write(
            '\x1b[2K\r' + "Searching all projects...")
        sys.stdout.flush()

        while diff > 0:
            response = self.api_call.request(
                constants.METHOD_GET, config.credential[constants.INSTANCE] + constants.ENDPOINT_PROJECT_SEARCH + "?startAt=" + str(start_at))
            
            status_code = response.status_code
            if response.status_code != constants.STATUS_CODE_OK:
                previous_diff = -1
                output = status_code
            else:
                response_json = response.json()
                values = response_json[constants.FIELD_VALUES]

                for info in values:
                    project_dict[info[constants.FIELD_NAME]] = info[constants.FIELD_KEY]
                    project_names.append(info[constants.FIELD_NAME])

                counter += 1
                previous_start = counter * 50
                previous_diff = response_json[constants.FIELD_TOTAL] - start_at

            start_at = previous_start
            diff = previous_diff
            
        if type(output) == int:
            return output
        else:
            # Prompt the project list to the user
            question_project_key = AskUser()
            project_key_selected = question_project_key.askQuestion("Choose a project to get issues", project_names)
            # Get the project key of the selected project
            output = project_dict[project_key_selected]

        return output

    # Function that writes the Epic row containing its data
    def setEpicRow(self,issue):
        return [issue[constants.FIELD_ID],
                issue[constants.FIELD_KEY],
                issue[constants.FIELD_FIELDS][constants.FIELD_ISSUETYPE][constants.FIELD_NAME],
                issue[constants.FIELD_FIELDS][constants.FIELD_SUMMARY],
                issue[constants.FIELD_FIELDS][constants.FIELD_TIMEESTIMATE],
                issue[constants.FIELD_FIELDS][constants.FIELD_TIMESPENT],
                issue[constants.FIELD_FIELDS][constants.FIELD_AGTIMEOST],
                issue[constants.FIELD_FIELDS][constants.FIELD_AGTS]]

    # Function that writes the Issue (task/story/bug/etc) row containing its data
    def setRowTask(self, issue, parent_epic):
        prefix = ''
        if parent_epic:
            prefix = '>'

        sys.stdout.write('\x1b[2K\r' + "Getting " + issue[constants.FIELD_FIELDS]
                         [constants.FIELD_ISSUETYPE][constants.FIELD_NAME] + ": " + issue[constants.FIELD_KEY])
        sys.stdout.flush()

        return [issue[constants.FIELD_ID],
                prefix + issue[constants.FIELD_KEY],
                issue[constants.FIELD_FIELDS][constants.FIELD_ISSUETYPE][constants.FIELD_NAME],
                issue[constants.FIELD_FIELDS][constants.FIELD_SUMMARY],
                issue[constants.FIELD_FIELDS][constants.FIELD_TIMEESTIMATE],
                issue[constants.FIELD_FIELDS][constants.FIELD_TIMESPENT],
                issue[constants.FIELD_FIELDS][constants.FIELD_AGTIMEOST],
                issue[constants.FIELD_FIELDS][constants.FIELD_AGTS]]

    # Function that writes the Sub-task row containing its data
    def setRowSubTask(self, subtask, response, parent_epic):
        prefix = '>'
        if parent_epic:
            prefix = '>>'
            
        sys.stdout.write('\x1b[2K\r' + "Getting Subtask: " +
                         subtask[constants.FIELD_KEY])
        sys.stdout.flush()

        original_time_value = 0
        original_time_field = response[constants.FIELD_FIELDS][constants.FIELD_TIMEOS]
        original_time_spent_value = 0
        original_time_spent_field = response[constants.FIELD_FIELDS][constants.FIELD_TIMESPENT]
        if original_time_field is not None:
            original_time_value = original_time_field
        if original_time_spent_field is not None:
            original_time_spent_value = original_time_spent_field

        return [subtask[constants.FIELD_ID],
                prefix +
                subtask[constants.FIELD_KEY],
                subtask[constants.FIELD_FIELDS][constants.FIELD_ISSUETYPE][constants.FIELD_NAME],
                subtask[constants.FIELD_FIELDS][constants.FIELD_SUMMARY],
                original_time_value,
                original_time_spent_value]

    # Function that gets the time tracking for the issue
    def getIssueTimeEstimation(self, issue_type, issue, parent_epic):
        rows = []

        # Does it have sub-tasks?
        if len(issue[constants.FIELD_FIELDS][constants.FIELD_SUBTASKS]) != 0:
            rows.append(self.setRowTask(issue, parent_epic))

            for subtask in issue[constants.FIELD_FIELDS][constants.FIELD_SUBTASKS]:
                response_sub_task = self.api_call.request(
                    constants.METHOD_GET, config.credential[constants.INSTANCE] + constants.ENDPOINT_ISSUE + subtask[constants.FIELD_KEY])
                response_subtask_json = response_sub_task.json()
                rows.append(self.setRowSubTask(subtask, response_subtask_json, parent_epic))

        else:
            rows.append(self.setRowTask(issue, parent_epic))
                        # rows.append(self.setRowTask(has_subtask, issue, parent_epic))

        return rows

    # Function that takes the project type, classic or next-gen
    def getProjectType(self, project_key):
        response = self.api_call.request(constants.METHOD_GET, config.credential[constants.INSTANCE] + constants.ENDPOINT_PROJECT + project_key)
        response_json = response.json()
        return response_json[constants.FIELD_SIMPLIFIED]

    # Function that gets the Sub-tasks inside the Epic, if there's one
    def getSubtasksFromEpic(self, epic, subtasks):
        rows = []
        for subtask in subtasks:
            response_sub_task = self.api_call.request(constants.METHOD_GET, config.credential[constants.INSTANCE] + constants.ENDPOINT_ISSUE + subtask[constants.FIELD_KEY])
            response_subtask_json = response_sub_task.json()
            rows.append(self.setRowSubTask(subtask, response_subtask_json, False))

        return rows

    # Funtion that search for the Epic Link in all projects and gets the time tracking data
    def getEpicLink(self, projects, epic):
        rows = []

        issue_only = self.setEpicRow(epic)

        all_subtasks_in_epic = epic[constants.FIELD_FIELDS][constants.FIELD_SUBTASKS]

        subtasks_in_epic = []
        if len(all_subtasks_in_epic) != 0:
            subtasks_in_epic = self.getSubtasksFromEpic(epic,all_subtasks_in_epic)
            
        total_estimation_epic = 0
        total_logged_epic = 0
        if issue_only[5] is not None:
            total_logged_epic = issue_only[5]

        if issue_only[4] is not None:
            total_estimation_epic = issue_only[4]

        rows.append(issue_only)

        epic_index = len(rows) - 1

        for row in subtasks_in_epic:
            rows.append(row)
            total_estimation_epic_subtask_unit = 0
            total_logged_epic_subtask_unit = 0

            if row[4] is not None:
                total_estimation_epic_subtask_unit = row[4]

            if row[5] is not None:
                total_logged_epic_subtask_unit = row[5]

            total_estimation_epic += total_estimation_epic_subtask_unit
            total_logged_epic += total_logged_epic_subtask_unit

        for project in projects:
            sys.stdout.write('\x1b[2K\r' + 'Searching for Epic Link of ' + epic[constants.FIELD_KEY] + " in the project " + project)
            sys.stdout.flush()
            is_next_gen = self.getProjectType(project)

            if self.validadeProjectKey(project.lower()):
                project = "'" + project + "'"

            epic_jql = ''
            valid = False
            if is_next_gen:
                epic_jql = "project = " + project + " and \"parentEpic\" = \"" + epic[constants.FIELD_KEY] + "\""
            else:
                if epic[constants.FIELD_FIELDS][constants.FIELD_EPICLINK] is not None:
                    epic_jql = "project = " + project + " and \"Epic Link\" = \"" + epic[constants.FIELD_KEY] + "\""
                    valid = True
                else: 
                    valid = False

            if valid:
                start_at = 0
                diff = 100
                counter = 0
                previous_start = 0
                previous_diff = 100

                while diff > 0:
                    json_query = {
                        'jql': epic_jql,
                        'startAt': start_at,
                        'maxResults': constants.MAX_RESULT,
                        'fields': constants.FIELDS
                    }

                    response = self.api_call.request(
                            constants.METHOD_POST, config.credential[constants.INSTANCE] + constants.ENDPOINT_SEARCH, json_data=json_query)

                    response_json = response.json()

                    status_code = response.status_code
                    if response.status_code != constants.STATUS_CODE_OK:
                        error = response.json()

                        sys.stdout.write('\x1b[2K\r' + 'Something went wrong: ' + str(
                            status_code) + ": " + error[constants.FIELD_ERROR_MESSAGES][0] + "\n")
                        sys.stdout.flush()
                        previous_diff = -1
                    else:
                        all_issues_in_epic = response_json[constants.FIELD_ISSUES]
                        
                        for issue in all_issues_in_epic:
                            issue_type = issue[constants.FIELD_FIELDS][constants.FIELD_ISSUETYPE][constants.FIELD_NAME]
                            child_issues_row = self.getIssueTimeEstimation(
                                issue_type, issue, True)

                            for row in child_issues_row:
                                total_estimation_epic_unit = 0
                                total_logged_epic_unit = 0

                                if row[4] is not None:
                                    total_estimation_epic_unit = row[4]

                                if row[5] is not None:
                                    total_logged_epic_unit = row[5]

                                total_estimation_epic += total_estimation_epic_unit
                                total_logged_epic += total_logged_epic_unit
                                rows.append(row)

                        rows[epic_index][6] = total_estimation_epic
                        rows[epic_index][7] = total_logged_epic

                        counter += 1
                        previous_start = counter * constants.MAX_RESULT
                        previous_diff = response_json[constants.FIELD_TOTAL] - start_at

                    start_at = previous_start
                    diff = previous_diff
        return rows

    # Not used for this case, debugging only
    def getIssues(self, instance, response, project):
        rows = []
        for issue in response:
            # Check if the issue is a subtask
            is_sub_task = issue[constants.FIELD_FIELDS][constants.FIELD_ISSUETYPE][constants.FIELD_SUBTASK]
            # Variable to indicate if the issue has sub-tasks

            if not is_sub_task:
                issue_type = issue[constants.FIELD_FIELDS][constants.FIELD_ISSUETYPE][constants.FIELD_NAME]
                # If is Epic
                if issue_type == 'Epic':
                    epic_jql = "project = " + project + " and \"Epic Link\" = \"" + \
                        issue[constants.FIELD_FIELDS]['customfield_10011'] + "\""

                    json_query_epic = {
                        'jql': epic_jql,
                        'startAt': 0,
                        'maxResults': 100
                    }

                    response_epic = self.api_call.request(
                        constants.METHOD_POST, instance + constants.ENDPOINT_SEARCH, json_data=json_query_epic)
                    response_json_epic = response_epic.json()

                    has_child = response_json_epic[constants.FIELD_ISSUES]

                    if len(has_child) == 0:
                        sys.stdout.write(
                            '\x1b[2K\r' + "Getting Epic: " + issue[constants.FIELD_KEY])
                        sys.stdout.flush()
                        time.sleep(0.5)
                        rows.append(self.setEpicRow(issue))
                    else:
                        sys.stdout.write(
                            '\x1b[2K\r' + "Getting Epic: " + issue[constants.FIELD_KEY] + " and its child issues")
                        sys.stdout.flush()
                        time.sleep(0.5)
                        issue_only = self.setEpicRow(issue)

                        total_estimation_epic = 0
                        total_logged_epic = 0
                        if issue_only[5] is not None:
                            total_logged_epic = issue_only[5]

                        if issue_only[4] is not None:
                            total_estimation_epic = issue_only[4]

                        rows.append(issue_only)
                        epic_index = len(rows) - 1

                        for issue in response_json_epic[constants.FIELD_ISSUES]:
                            child_issues_row = self.getIssueTimeEstimation(
                                issue_type, issue, True)

                            for row in child_issues_row:
                                total_estimation_epic_unit = 0
                                total_logged_epic_unit = 0

                                if row[4] is not None:
                                    total_estimation_epic_unit = row[4]

                                if row[5] is not None:
                                    total_logged_epic_unit = row[5]

                                total_estimation_epic += total_estimation_epic_unit
                                total_logged_epic += total_logged_epic_unit
                                rows.append(row)

                        rows[epic_index][6] = total_estimation_epic
                        rows[epic_index][7] = total_logged_epic

                # If is not Epic
                elif issue_type == 'Initiative':
                    pass
                else:
                    if issue[constants.FIELD_FIELDS][constants.FIELD_EPICLINK] is None:
                        issues_rows = self.getIssueTimeEstimation(
                            issue_type, issue, False)
                        for row in issues_rows:
                            rows.append(row)

        return rows

    # Function that checks if the project key does not belong to the reserved words for the JQL
    def validadeProjectKey(self, key):
        if key in constants.RESERVED_WORDS:
            return True
        else: 
            return False
