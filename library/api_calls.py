# main.py
# Epic Time Tracking
# api_calls.py
# Create by Fábio Beck Wanderer

import requests
import json
from requests.auth import HTTPBasicAuth

class ApiCalls():
    auth_method = 'basic'
    auth = None
    cookie_jar = None
    username = None
    password = None

    def setCredentials(self, config):
        self.username = config['username']
        self.password = config['password']

    def setAuth(self, auth_method):
        if auth_method == 'basic':
            self.auth = HTTPBasicAuth(self.username, self.password)
        elif auth_method == 'cookie':
            raise TypeError("Cookie auth method not yet implemented")
        elif auth_method == 'oauth':
            raise TypeError("OAuth method not yet implemented")
        else:
            raise TypeError("Unknown auth type " + auth_method + ".")
        
        self.auth_method = auth_method
    
    def request(self, request_type, url, json_data={}, url_params={}, request_headers={}):
        
        headers = {'Content-Type': 'application/json'}
        headers.update(request_headers)
        self.r = None
        
        try:
            if request_type == "GET":
                self.r = requests.get(url, json=json_data, params=url_params, auth=self.auth)
            elif request_type == "PUT":
                self.r = requests.put(url, json=json_data, params=url_params, auth=self.auth, headers=headers)
            elif request_type == "POST":
                self.r = requests.post(url, json=json_data, params=url_params, auth=self.auth, headers=headers)
            elif request_type == "DELETE":
                self.r = requests.delete(url, json=json_data, params=url_params, auth=self.auth, headers=headers)
            elif request_type == "HEAD":
                self.r = requests.head(url, json=json_data, params=url_params, auth=self.auth, headers=headers)
            elif request_type == "OPTIONS":
                self.r = requests.options(url, json=json_data, params=url_params, auth=self.auth, headers=headers)
            else:
                raise TypeError("Unknown "+request_type+" request type")
        finally:
            self.last_status_code = self.r.status_code
            
            return self.r
