# main.py
# Epic Time Tracking
# config.py
# Create by Fábio Beck Wanderer

# Instructions
# Fill the value of the 'instance' key with your instance URL
# Username should be your atlassian account e-mail
# Password should be your token
# Important things to keep in mind:
# - The user of the script must have all the necessary permissions for searching for issues on the target projects
# - The URL of the instance must start with 'https://'
# - The user must generate a token. More information information can be found here: https://confluence.atlassian.com/cloud/api-tokens-938839638.html

credential = {
    'instance': 'https://<instance>.atlassian.net/',
    'username': '<e-mail>',
    'password': '<api_token>'
}

# Name for the generated report
filename = 'report'
